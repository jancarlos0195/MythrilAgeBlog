---
title: "About"
date: 2023-07-05T15:43:43-04:00
draft: false
layout: about
---

## Introduction

Hello there! I'm Jan Carlos Domínguez, a software engineer based in Raleigh, NC, United States. I specialize in back-end development and have a strong passion for creating innovative web applications. Welcome to my blog, where I share insights and experiences from my professional journey as a software engineer.

## Background and Skills

With four years of experience in the field, I bring a creative and passionate approach to back-end engineering. I hold a certificate in Full Stack Web Development from the University of North Carolina at Charlotte and have a strong foundation in object-oriented programming, as well as proficiency in Java, C#, Python, and PostgresSQL.

Throughout my career, I have proven myself to be a quick learner and a dedicated team player. I thrive both independently, effectively managing projects, and in collaborative team settings, where I contribute my skills to achieve exceptional outcomes.

## Interests and what to expect from here

Beyond my professional life, I have a wide range of interests. I am an avid fan of video games, particularly RPGs, which have influenced my passion for technology and problem-solving. I also enjoy traveling, exploring different cultures, and gaining new perspectives. Art, philosophy, martial arts, literature, and adventure are among my other hobbies that inspire me and contribute to my personal growth.

## Get in Touch

Thank you for visiting my blog and taking the time to learn more about me. If you have any questions, comments, or would like to connect, feel free to reach out to me at jancarlos0195@gmail.com. You can also find me on GitHub ([JanInquisitor](https://github.com/JanInquisitor)) and GitLab ([jancarlos0195](https://gitlab.com/jancarlos0195)).

I look forward to sharing valuable insights and experiences with you through my blog. Stay tuned for more!
